import React, {
    Fragment,
    useState,
    useEffect,
    useReducer,
} from 'react';
import axios from 'axios';
import './App.css';


const dataFetchReducer = (state, action) => {
    switch (action.type) {
        case 'FETCH_INIT':
            return { ...state, isLoading: true, isError: false };
        case 'FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
            };
        case 'FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:
            throw new Error();
    }
};

const useDataApi = (initialUrl, initialData) => {
    const [url, setUrl] = useState(initialUrl);

    const [state, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        isError: false,
        data: initialData,
    });

    useEffect(() => {
        let didCancel = false;

        const fetchData = async () => {
            dispatch({ type: 'FETCH_INIT' });

            try {
                const result = await axios(url);

                if (!didCancel) {
                    dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
                }
            } catch (error) {
                if (!didCancel) {
                    dispatch({ type: 'FETCH_FAILURE' });
                }
            }
        };
        if (url !== '') {
            fetchData();
        }
        return () => {
            didCancel = true;
        };
    }, [url]);

    return [state, setUrl];
};


function App1() {
    const [checked, setChecked] = useState(false);
    const [number, setNumber] = useState(null);
    const [code, setCode] = useState(null);

    const [{ data, isLoading }, doFetch] = useDataApi('', null);

    return (
        <div className="App">
            <div className="gift-box">
                <div className="gift-head">Gift Cards</div>
                <div>
                    <label className="gift-checkbox">
                        <input type="checkbox" checked={checked} onChange={() => setChecked(!checked)}/>
                        <span className="checkmark"/>
                        <span>Do you have a gift card?</span>
                    </label>
                </div>
                {checked && <Fragment>
                    <form
                        onSubmit={event => {
                            doFetch(`api/v1/data?number=${number}&code=${code}`);
                            event.preventDefault();
                        }}
                    >
                        <div className="gift-hint">Please enter the 19-digit number and code from your gift card
                            below.
                        </div>
                        {data !== null && <div className="received-data">
                            <div>
                                <div> Gift Card</div>
                                <span style={{ fontWeight: '300' }}>**** **** **** **** 123</span>
                            </div>
                            <span style={{ fontWeight: 'bold' }}>-€20.00</span>
                        </div>
                        }
                        {isLoading && <div className="received-data-loading">Loading...</div>}
                        <div className="gift-codes">
                            <input
                                type="number"
                                className="gift-input"
                                style={{ width: '330px' }}
                                placeholder="0000 1234 5678 9012 345"
                                value={number}
                                onChange={(event) => setNumber(event.target.value)}
                            />
                            <input
                                type="number"
                                className="gift-input"
                                placeholder="001"
                                value={code}
                                onChange={(event) => setCode(event.target.value)}
                            />
                        </div>

                        <button type="submit" className="gift-button">
                            <span className="gift-button-text">APPLY</span>
                        </button>
                    </form>
                </Fragment>}
            </div>
        </div>
    );
}

export default App1;
