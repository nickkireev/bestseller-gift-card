## ReactJS, Hooks, create-react-app

BESTSELLER.COM 

### Case Study Shopping Cart 

**Task:**

Bestseller Frontend Test

####Spec

- We ask you to create an app based on the mockup (you can find it in
`./design` ).

- It's a form that allows users to apply their gift card to get a discount.

- It starts with a single checkbox label. When you select it — you see the
small form with two input
fields and the apply button.

- Gift cards require validation (only numbers and the security code).

- In the real life it requires verification so we would like to ask to implement
some fake check.

- no need to do build real backend for it (as it's frontend position), you
can use something like `json—server`

If gift card passes the verification — show the result of applying for this card

#####Notes

- You can find design mockup in `./design` folder (there are `Figma` design
file and `.png` )

- Please, use `React`

- Do not spend too much time.

### Spent time

2,5 hours
 
### Run project 

```sh
npm ci
npm start
```

### Developer
[@nickkireev]

[@nickkireev]: <https://bitbucket.org/nickkireev>
